import { Column, Entity, PrimaryGeneratedColumn, UpdateQueryBuilder } from 'typeorm';
import { QueryBuilderStubber } from './query-builder-stubber';
import Stubber from './stubs';

@Entity()
class Product {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @Column({
        type: 'varchar'
    })
    price!: number;
}

describe('Test query builder stubber', () => {
    let serviceSandbox: sinon.SinonSandbox;

    const queryBuilderStubber = new QueryBuilderStubber();

    after(() => {
        sinon.restore();
    });

    describe('Test query builder stubbing', () => {
        beforeEach(() => {
            serviceSandbox = sinon.createSandbox();
        });

        afterEach(() => {
            serviceSandbox.restore();
        });

        it('should generate query builder stub', async () => {
            const stubs = Stubber.stub(Product);
            const stubRaws = Stubber.stubRaw(Product);

            const updateQueryBuilderStub = sinon.createStubInstance(UpdateQueryBuilder);

            serviceSandbox
                .stub(queryBuilderStubber, '_stubUpdateQueryBuilder')
                .returns(updateQueryBuilderStub);

            const productQueryBuilderStub = queryBuilderStubber._stubQueryBuilder(
                sinon,
                stubs,
                stubRaws
            );

            expect(productQueryBuilderStub).ok;

            productQueryBuilderStub
                .select(['product.id', 'product.name'])
                .addSelect('COUNT(product.id)', 'productCounts')
                .from(Product, 'product')
                .addFrom(Product, 'secondProduct')
                .leftJoin('product.users', 'user')
                .leftJoinAndSelect('product.productDetail', 'productDetail')
                .where('product.id = :productId', { productId: 1 })
                .andWhere('product.name = :productName', { productName: 'Ramen' })
                .andWhere('product.price > :productPrice')
                .skip(1)
                .take(2)
                .setParameter('productPrice', 10);

            const loadedProduct = await productQueryBuilderStub.getOne();
            const loadedProducts = await productQueryBuilderStub.getMany();
            const loadedRawProduct = await productQueryBuilderStub.getRawOne();
            const loadedRawProducts = await productQueryBuilderStub.getRawMany();

            expect(loadedProduct).ok;
            expect((loadedProduct as Product).id).ok;
            expect(loadedRawProduct.product_id).ok;

            expect(loadedProducts).ok;
            expect(loadedProducts.length).greaterThan(0);

            expect(loadedRawProducts).ok;
            expect(loadedRawProducts.length).greaterThan(0);
        });

        it('should generate update query builder stub', async () => {
            const updateQueryBuilderStub = queryBuilderStubber._stubUpdateQueryBuilder(sinon);

            const result = await updateQueryBuilderStub
                .set(2)
                .where('product.id = :productId', { productId: 1 })
                .execute();

            expect(result).ok;
            expect(result.affected).ok;
        });
    });
});
